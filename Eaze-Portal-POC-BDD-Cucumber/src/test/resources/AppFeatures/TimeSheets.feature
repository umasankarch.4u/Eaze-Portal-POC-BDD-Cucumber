Feature: TimeSheets Page Feature

  Scenario: Verify Time Sheet is saved as Draft in Week-2
    Given user is on login page
    When user finds disclaimer message
    And Click on Accept & Continue button
    And user enters Url as "motivity"
    And user enters Username as "umasankar.chilumuri@motivitylabs.com"
    And user enters Password as "Shiva@34366"
    And user clicks on "Login"
    And User should login to the Eaze portal
    And Title of the Page should be "EazeWork : Dashboard"
    And verify that Timesheet option is available in left navigation
    Then verify that the user able to click on timesheet button
    And verify that the user naviagte to Timesheets Page
    And verify that the user is able to select current week timesheet
    And verify that the user is able to log hours in timesheet form
    And verify that user is able to save the entered timesheet details as a draft
