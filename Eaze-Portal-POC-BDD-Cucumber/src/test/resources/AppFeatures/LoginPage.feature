Feature: Login Page Feature

#Scenario: Login Page Title Validation
#Given user is on login page
#When user gets the title of the page
#Then Page title should be "Login to EazeWork - Business Applications for SMEs"
#
#Scenario: Forgot Password link
#Given user is on login page
#Then forgot password link should be displayed 
  #
  #
 #Scenario: Close Disclaimer by clicking Accept & Continue
#Given user is on login page
#When user finds disclaimer message
#Then Click on Accept & Continue button
#
#Scenario: Close Disclaimer by clicking Close Button
#Given user is on login page
#When user finds disclaimer message
#Then Click on Close button

Scenario: Click submit without URL Username and Password
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as ""
And user enters Username as ""
And user enters Password as ""
And user clicks on "Login"
Then User should be notified with No URL message as "Enter URL"
And User should be notified with No Username message as "Enter username"
And User should be notified with No Password message as "Enter password"

Scenario: Click submit without Username and Password
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as ""
And user enters Password as ""
And user clicks on "Login"
Then User should be notified with No Username message as "Enter username"
And User should be notified with No Password message as "Enter password"

Scenario: Click submit without Password
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as "umasankar.chilumuri1@motivitylabs.com"
And user enters Password as ""
And user clicks on "Login"
Then User should be notified with No Password message as "Enter password"

Scenario: Login with Invalid URL
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motiviti"
And user enters Username as "umasankar.chilumuri1@motivitylabs.com"
And user enters Password as "HelloWorld"
And user clicks on "Login"
Then User should be notified with invalid Org message as "Not a valid url"

Scenario: Login with Invalid User
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as "umasankar.chilumuri1@motivitylabs.com"
And user enters Password as "HelloWorld"
And user clicks on "Login"
Then User should be notified with invalid User message as "User does not belong to company"

Scenario: Login with Invalid Password-First Attempt
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as "umasankar.chilumuri@motivitylabs.com"
And user enters Password as "HelloWorld"
And user clicks on "Login"
Then User should be notified with invalid User message as "You have entered wrong password."

Scenario: Login with Invalid Password-Second Attempt
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as "umasankar.chilumuri@motivitylabs.com"
And user enters Password as "HelloWorld"
And user clicks on "Login"
Then User should be notified with invalid User message as "You have entered wrong password 2 times, only 3 attempts are left before your account would be locked."

Scenario: Login with Invalid Password-Third Attempt
Given user is on login page
When user finds disclaimer message
Then Click on Accept & Continue button
When user enters Url as "motivity"
And user enters Username as "umasankar.chilumuri@motivitylabs.com"
And user enters Password as "HelloWorld"
And user clicks on "Login"
Then User should be notified with invalid User message as "You have entered wrong password 3 times, only 2 attempts are left before your account would be locked."