package stepdefinitions;

import com.pages.HomePage;
import com.qa.factory.DriverFactory;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import java.io.File;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HomePageSteps {

    private static String title;
    private HomePage homePage = new HomePage(DriverFactory.getDriver());

    @Then("User should login to the Eaze portal")
    public void user_should_login_to_the_eaze_portal() throws InterruptedException {
        Assert.assertTrue(homePage.homePageLogoExist());
    }

    @Then("Title of the Page should be {string}")
    public void title_of_the_page_should_be(String expectedTitleName) throws InterruptedException {

        title = homePage.getHomePageTitle();

        Assert.assertTrue(title.contains(expectedTitleName));
        System.out.println("Page title is:" + title);
        Thread.sleep(1000);
    }

    @Then("verify that Attendance calendar is displayed")
    public void verify_that_attendance_calendar_is_displayed() {
        Assert.assertTrue(homePage.attendanceCalendarExist());
    }

    @Then("Verify if user is present on the current date")
    public void verify_if_user_is_present_on_the_current_date() throws InterruptedException {
        LocalDate dateObj = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateDDMMYYYY = dateObj.format(formatter);
        homePage.clickAnyDateLabel(dateDDMMYYYY);
        Thread.sleep(1000);
        String Status = homePage.checkEmployeeAttendanceStatus(dateDDMMYYYY);

        if (Status.equals("Present")) {
            System.out.println("Employee is Present today ");
        } else {

            System.out.println("Employee is not Present today " + Status);
        }
        homePage.clickOnCloseBtn();
        Thread.sleep(1000);
    }

    @Then("Report attendance report of employee for past ten days")
    public void report_attendance_report_of_employee_for_past_ten_days() throws IOException, InterruptedException {

        this.getPastTenDaysData();
        this.writeDataToExcel();
    }

    ArrayList statusArray = new ArrayList();
    ArrayList datesArray = new ArrayList();

    public void getPastTenDaysData() throws InterruptedException {

        //String currentDate= homePage.returnCurrentDate();

        for (int i = 0; i <= 10; i++) {
            LocalDate dateObj = LocalDate.now();
            LocalDate dateNew = dateObj.minusDays(i);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String dateDDMMYYYYNew = dateNew.format(formatter);
            homePage.clickAnyDateLabel(dateDDMMYYYYNew);
            Thread.sleep(1000);
            String Status = homePage.checkEmployeeAttendanceStatus(dateDDMMYYYYNew);
            datesArray.add(dateDDMMYYYYNew);
            statusArray.add(Status);
            Thread.sleep(1000);
            homePage.clickOnCloseBtn();
            Thread.sleep(1000);
        }
    }

    public void writeDataToExcel() throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadsheet
                = workbook.createSheet(" Employee Attendance Data ");

        // creating a row object
        XSSFRow row;

        // This data needs to be written (Object[])
        Map<Integer, Object[]> empData
                = new TreeMap<Integer, Object[]>();

        /*empData.put(
                1,
                new Object[]{"Date", "Att Status"});*/

        for (int k = 0; k <= datesArray.size(); k++) {
            if (k == 0) {
                empData.put(k, new Object[]{"Date", "Attendance Status"});
            } else {
                empData.put(k, new Object[]{datesArray.get(k - 1), statusArray.get(k - 1)});
            }
        }

        Set<Integer> keyid = empData.keySet();
        int rowid = 0;

        // writing the data into the sheets...

        for (Integer key : keyid) {

            row = spreadsheet.createRow(rowid++);
            Object[] objectArr = empData.get(key);
            int cellid = 0;

            for (Object obj : objectArr) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String) obj);
            }
        }
        // System.out.println("Step Executing Flag");
        LocalDateTime dateObj2 = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH_mm_ss");
        String dateDDMMYYYYNew = dateObj2.format(formatter);
        FileOutputStream out = new FileOutputStream(
                new File("D:/Attendance Reports/EmpAttSheet_" + dateDDMMYYYYNew + ".xlsx"));

        workbook.write(out);
        out.close();
        // System.out.println("Data written to file");
    }

}
