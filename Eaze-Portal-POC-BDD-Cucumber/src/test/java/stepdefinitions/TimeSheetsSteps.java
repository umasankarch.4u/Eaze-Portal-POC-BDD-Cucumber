package stepdefinitions;

import com.pages.HomePage;
import com.pages.TimeSheets;
import com.qa.factory.DriverFactory;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TimeSheetsSteps {
    private TimeSheets timeSheet = new TimeSheets(DriverFactory.getDriver());
    @When("verify that Timesheet option is available in left navigation")
    public void verify_that_timesheet_option_is_available_in_left_navigation() throws InterruptedException {
       Assert.assertTrue(timeSheet.timeSheetBtnDisplayed());
       Thread.sleep(500);
    }

    @Then("verify that the user able to click on timesheet button")
    public void verify_that_the_user_able_to_click_on_timesheet_button() throws InterruptedException {
        timeSheet.clickTimeSheetsBtn();
        Thread.sleep(500);
    }
    @Then("verify that the user naviagte to Timesheets Page")
    public void verify_that_the_user_naviagte_to_timesheets_page() throws InterruptedException {
        timeSheet.timeSheetPageDisplayed();
        System.out.println("Navigated to Manage Timesheets Page");
    }

    @Then("verify that the user is able to select current week timesheet")
    public void verify_that_the_user_is_able_to_select_current_week_timesheet() throws InterruptedException {
       // timeSheet.clickOnNewTimeSheetBtn();
        //Assert.assertTrue(timeSheet.selectWeekToLogHours());

        timeSheet.clickSavedDraftTimeSheetBtn();
        System.out.println("Week Selected");

    }

    @Then("verify that the user is able to log hours in timesheet form")
    public void verify_that_the_user_is_able_to_log_hours_in_timesheet_form() throws InterruptedException {
        timeSheet.selectProjectActivity();
    }

    @Then("verify that user is able to save the entered timesheet details as a draft")
    public void verify_that_user_is_able_to_save_the_entered_timesheet_details_as_a_draft() throws InterruptedException {
        Assert.assertTrue(timeSheet.clickOnSaveAsDraft());
    }

}
