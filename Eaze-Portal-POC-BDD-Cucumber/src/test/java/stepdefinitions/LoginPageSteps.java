package stepdefinitions;

import org.junit.Assert;

import com.pages.LoginPage;
//import com.pages.HomePage;
import com.qa.factory.DriverFactory;

import io.cucumber.core.exception.CucumberException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginPageSteps {
    private static String title;
    private LoginPage loginPage = new LoginPage(DriverFactory.getDriver());
    //private HomePage homePage = new HomePage(DriverFactory.getDriver());

    @Given("user is on login page")
    public void user_is_on_login_page() throws InterruptedException {

        DriverFactory.getDriver().get("https://www.eazework.com/login/Default.aspx");
        Thread.sleep(1000);
    }

    @When("user gets the title of the page")
    public void user_gets_the_title_of_the_page() throws InterruptedException {

        title = loginPage.getLoginPageTitle();
        System.out.println("Page title is:" + title);
        Thread.sleep(1000);

    }

    @Then("Page title should be {string}")
    public void page_title_should_be(String expectedTitleName) throws InterruptedException {

        Assert.assertTrue(title.contains(expectedTitleName));
        Thread.sleep(1000);

    }

    @Then("forgot password link should be displayed")
    public void forgot_password_link_should_be_displayed() throws InterruptedException {
        Assert.assertTrue(loginPage.isForgotPwdLinkExist());
        Thread.sleep(1000);
    }

    @When("user enters Url as {string}")
    public void user_enters_url_as(String urlName) throws InterruptedException {
        loginPage.enterUrlName(urlName);
        Thread.sleep(1000);

    }

    @When("user enters Username as {string}")
    public void user_enters_username_as(String userName) throws InterruptedException {
        loginPage.enterUserName(userName);
        Thread.sleep(1000);

    }

    @When("user enters Password as {string}")
    public void user_enters_password_as(String pwd) throws InterruptedException {
        loginPage.enterPassword(pwd);
        Thread.sleep(1000);

    }

    @When("user clicks on {string}")
    public void user_clicks_on(String string) throws InterruptedException {

        loginPage.clickOnLogin();
        Thread.sleep(2000);
        if (loginPage.verifyIfAlreadyLoggedIn()) {
           // Assert.assertTrue(homePage.homePageLogoExist());
        } else {

            loginPage.clickOnForceLogin();
            Thread.sleep(1000);
        }



    }

    @Then("User should be notified with invalid Org message as {string}")
    public void user_should_be_notified_with_invalid_Org_message_as(String invalidMsg) throws InterruptedException {

            if (loginPage.verifyInvalidurl()) {
                Assert.assertTrue(loginPage.verifyInvalidurl());
                System.out.println("User gets a warning message as:" + invalidMsg);
                Thread.sleep(500);
            }


    }

    @Then("User should be notified with invalid User message as {string}")
    public void user_should_be_notified_with_invalid_user_message_as(String invalidMsg1) throws InterruptedException {

        if (loginPage.verifyInvalidUser()) {
            Assert.assertTrue(loginPage.verifyInvalidUser());
            System.out.println("User gets a warning message as:" + invalidMsg1);
            Thread.sleep(500);
        }

    }

    @When("user finds disclaimer message")
    public void user_finds_disclaimer_message() {
        loginPage.verifyDisclaimerDisplay();
    }

    @Then("Click on Close button")
    public void click_on_continue_button() {
        loginPage.clickOnDisclaimerClose();
    }

    @Then("Click on Accept & Continue button")
    public void click_on_accept_continue_button() {
        loginPage.clickOnDisclaimerAccept();
    }

    @Then("User should be notified with No URL message as {string}")
    public void user_should_be_notified_with_no_url_message_as(String string) {
        loginPage.msgEnterURLDisplayed();
    }

    @Then("User should be notified with No Username message as {string}")
    public void user_should_be_notified_with_no_username_message_as(String string) {
        loginPage.msgEnterUsernameDisplayed();

    }

    @Then("User should be notified with No Password message as {string}")
    public void user_should_be_notified_with_no_password_message_as(String string) {
        loginPage.msgEnterPasswordDisplayed();

    }

}
