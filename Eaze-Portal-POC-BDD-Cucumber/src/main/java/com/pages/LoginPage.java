package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.sql.SQLOutput;

import static com.qa.util.Constants.SPAN_MESSAGE;

public class LoginPage {

	private WebDriver driver;
	// 1. By locators

	private By url = By.id("txtCorpURL");
	private By userName = By.id("txtLogin");
	private By password = By.id("Password");
	private By loginBtn = By.id("btnLogin");

	//private By forceLoginBtn= By.id("btnLogin");

	private By invalidOrg = By.xpath(SPAN_MESSAGE);;
	private By invalidUser = By.xpath(SPAN_MESSAGE);
	//private By     = By.xpath("//span[@data-notify=\"message\"]");
	private By forgotPwdLink = By.linkText("I forgot my login credentials?");
	private By disclaimerWindow = By.xpath("//p[@class=\'cookiePolicyMessage\']");
	private By disclaimerAccept = By.xpath("//input[@value='ACCEPT & CONTINUE']");
	private By disclaimerClose = By.xpath("//input[@value='CLOSE']");
	private By msgEnterURL=By.xpath(SPAN_MESSAGE);;
	private By msgEnterUsername=By.xpath(SPAN_MESSAGE);;
	private By msgEnterPassword=By.xpath(SPAN_MESSAGE);

	private By alreadyLoggedInMessage;
	

	// 2. Constructor of the Page class
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	// 3. Page actions--Features

	public String getLoginPageTitle() {
		return driver.getTitle();
	}

	public boolean isForgotPwdLinkExist() {
		return driver.findElement(forgotPwdLink).isDisplayed();
	}

	public void enterUrlName(String urlName) {
		driver.findElement(url).sendKeys(urlName);
	}

	public void enterUserName(String user) {
		driver.findElement(userName).sendKeys(user);
	}

	public void enterPassword(String pwd) {
		driver.findElement(password).sendKeys(pwd);
	}

	public void clickOnLogin() {


		driver.findElement(loginBtn).click();

	}

	public void clickOnForceLogin() {
			driver.findElement(loginBtn).click();
			System.out.println("Unable to find Login button, Force login executing");

	}

	public boolean verifyInvalidurl() {
		return driver.findElement(invalidOrg).isDisplayed();
	}

	public boolean verifyInvalidUser() {
		return driver.findElement(invalidUser).isDisplayed();
	}

	public boolean verifyDisclaimerDisplay() {
		return driver.findElement(disclaimerWindow).isDisplayed();
	}

	public void clickOnDisclaimerClose() {
		driver.findElement(disclaimerClose).click();

	}
	public void clickOnDisclaimerAccept() {
		driver.findElement(disclaimerAccept).click();

	}
	
	public boolean msgEnterURLDisplayed() {
		return driver.findElement(msgEnterURL).isDisplayed();
	}
	public boolean msgEnterUsernameDisplayed() {
		return driver.findElement(msgEnterUsername).isDisplayed();
	}

	public boolean msgEnterPasswordDisplayed() {
		return driver.findElement(msgEnterPassword).isDisplayed();
	}
	public boolean verifyIfAlreadyLoggedIn() {
		String message="You are already logged into the application or your previous session would not have closed properly. If you click on Force Login you will be logged out from the other session. Any unsaved work there will be lost.";
		alreadyLoggedInMessage= By.xpath(SPAN_MESSAGE);
		String message2 = driver.findElement(alreadyLoggedInMessage).getText();
		if(message==message2) {
			return true;
		}
		else{
			return false;
		}
	}
}
