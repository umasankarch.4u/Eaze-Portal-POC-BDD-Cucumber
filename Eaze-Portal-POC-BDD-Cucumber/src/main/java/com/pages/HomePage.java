package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage {

    private WebDriver driver;
    // 1. By locators


    private By homePageLogo = By.id("imageLogo");
    private By attendanceCalendar = By.id("ATS_AttendanceCalendar");
    String date, dateNew;

    // private By isPresentOnCurrentDate = By.xpath("//*[@id=\"lblAttendanceStatus\"]/table/tbody/tr[1]/td[2]");
    private By attendanceStatusonDate = By.xpath("//*[@id=\"lblAttendanceStatus\"]/table/tbody/tr[1]/td[2]");
    //private By attDetailsWindowOpen = By.xpath("//h5[@class=\"modal-title\"]");
    private By closeButton = By.xpath("//button[@onclick=\"hidePop('AttCalPopup');\"]/span");
    private By dateLabel;

    // 2. Constructor of the Page class
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    // 3. Page actions--Features

    public String getHomePageTitle() {
        return driver.getTitle();
    }

    public boolean homePageLogoExist() {
        return driver.findElement(homePageLogo).isDisplayed();
    }

    public boolean attendanceCalendarExist() {
        return driver.findElement(attendanceCalendar).isDisplayed();
    }

    public void currentDateLabel(String dateDDMMYYYY) {
        date = dateDDMMYYYY;
        dateLabel = By.xpath("//td[@onclick=\"showAttCalPopup('" + date + "')\"]");
        driver.findElement(dateLabel).click();
    }

/*    public boolean clickOnCurrentDate(String dateDDMMYYYY) {
        date = dateDDMMYYYY;
        dateLabel = By.xpath("//td[@onclick=\"showAttCalPopup('" + date + "')\"]");
      //  driver.findElement(dateLabel).click();
        driver.findElement(attDetailsWindowOpen).isDisplayed();
        return driver.findElement(isPresentOnCurrentDate).isDisplayed();
    }*/

    public void clickOnCloseBtn() {
        driver.findElement(closeButton).click();
    }

    public void clickAnyDateLabel(String i) {
        dateNew = i;
        dateLabel = By.xpath("//td[@onclick=\"showAttCalPopup('" + dateNew + "')\"]");
        driver.findElement(dateLabel).click();
    }

    public String checkEmployeeAttendanceStatus(String i) {

        //  attendanceStatusonDate= By.id("lblAttendanceStatus");
        driver.findElement(attendanceStatusonDate).isDisplayed();
        String status = driver.findElement(attendanceStatusonDate).getText();
        System.out.println("Attendance Status is " + status + " on " + i);
        return (status);

    }

}
