package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.qa.util.Constants.SPAN_MESSAGE;


public class TimeSheets {

    private WebDriver driver;
    // 1. By locators
    private By timesheetsLeftNavBtn = By.id("11");
    private By timesheetsPageTitle = By.id("litab1");
    private By newTimeSheetBtn = By.id("btNewTimesheet");
    private By selectSavedDraftTimeSheetBtn= By.id("lblDraftCount");
    private By selectDraftedTimeSheet = By.id("lnkTSDet_0");
    private By weekSelectionWindowDisplayed =By.xpath("//*[@class=\"modal-title\"]");
    private By weekSelectionBtn= By.xpath("//*[@id=\"ddlNewTS\"]/option[1]");
    private By nextBtn= By.id("btSubmitNTS");
    private By editTimeSheetWindowTitle= By.id("spanTSHeading1");
  // private By displayeddWeekTitle= By.xpath("//*[@id=\"spanTSHeading2\"]");
    private By displayeddWeekTitle= By.id("spanTSHeading2");


    private By selectProjectBtn= By.id("lbPrjF");
    private By selectActivityBtn= By.id("lbCCF");
    private By clickOnChooseProjectLOV= By.xpath("//a[@class=\"chosen-single chosen-default\"]/span[contains(text(), \"Select Project\")]");
    private By clickOnChooseActivityLOV= By.xpath("//a[@class=\"chosen-single chosen-default\"]/span[contains(text(), \"Select Activity\")]");
    private By projectActivityWindowDisplayed= By.xpath("//h5[@class=\"modal-title\"]");
    private By clickProjectNameBtn= By.xpath("//li[contains(text(),\"(ML-045) Digital Reef_Data Rewards\")]");
    private By clickActivityGrooming= By.xpath("//li[contains(text(),\"Grooming\")]");
    private By clickActivityDailyScrum= By.xpath("//li[contains(text(),\"Daily Scrum\")]");
    private By clickActivityIterationExecution= By.xpath("//li[contains(text(),\"Itetration Execution- QA\")]");
    private By clickActivityLeave= By.xpath("//li[contains(text(),\"Leave\")]");
    private By clickActivityHoliday= By.xpath("//li[contains(text(),\"Holiday\")]");
    private By clickSaveBtn= By.id("btSavePrjCCPop");
    private By clickRowSaveBtn = By.xpath("//a[@class=\"btn btn-info btn-xs\"]");
    private By clickSaveasDraftBtn= By.id("btDraft");

    private By msgDataSaved=By.xpath(SPAN_MESSAGE);;




    // 2. Constructor of the Page class
    public TimeSheets(WebDriver driver) {
        this.driver = driver;
    }

    // 3. Page actions--Features
    public boolean timeSheetBtnDisplayed() {
       return driver.findElement(timesheetsLeftNavBtn).isDisplayed();
    }
    public void clickTimeSheetsBtn() throws InterruptedException {
         driver.findElement(timesheetsLeftNavBtn).click();
         Thread.sleep(1000);
    }
    public boolean timeSheetPageDisplayed() {
        return driver.findElement(timesheetsPageTitle).isDisplayed();
    }
    public void clickOnNewTimeSheetBtn() throws InterruptedException {
        driver.findElement(newTimeSheetBtn).click();
        Thread.sleep(500);
        driver.findElement(weekSelectionWindowDisplayed).isDisplayed();
    }
    public void clickSavedDraftTimeSheetBtn() throws InterruptedException {

        driver.findElement(selectSavedDraftTimeSheetBtn).click();
        Thread.sleep(500);
        driver.findElement(selectDraftedTimeSheet).click();
        Thread.sleep(500);

    }
    public boolean selectWeekToLogHours() throws InterruptedException {
        driver.findElement(weekSelectionBtn).click();
        String selectedWeekFull= driver.findElement(weekSelectionBtn).getText();
        String selectedWeek = selectedWeekFull.substring(0,5);
       Thread.sleep(900);
        driver.findElement(nextBtn).click();
        driver.findElement(editTimeSheetWindowTitle).isDisplayed();
        Thread.sleep(500);
        String displayedWeek=driver.findElement(displayeddWeekTitle).getText();
        //System.out.println("++++++++++++++++" +displayedWeek);
        Thread.sleep(500);
       // System.out.println("Selected"+selectedWeek +"Displayed" +displayedWeek);
        System.out.println("Selected time sheet for:" +selectedWeekFull +" week");
        return displayedWeek.contains(selectedWeek);

    }
    public void selectProjectActivity() throws InterruptedException {
        // Grooming
        driver.findElement(selectProjectBtn).click();
       Thread.sleep(900);
        driver.findElement(projectActivityWindowDisplayed).isDisplayed();
        driver.findElement(clickOnChooseProjectLOV).click();
       Thread.sleep(900);
        driver.findElement(clickProjectNameBtn).click();
       Thread.sleep(900);
        driver.findElement(clickOnChooseActivityLOV).click();
        driver.findElement(clickActivityGrooming).click();
       Thread.sleep(900);
        driver.findElement(clickSaveBtn).click();
        driver.findElement(clickRowSaveBtn).click();
        Thread.sleep(900);
        System.out.println("Grooming");

        // Daily Scrum call
        driver.findElement(selectProjectBtn).click();
       Thread.sleep(900);
        driver.findElement(projectActivityWindowDisplayed).isDisplayed();
        driver.findElement(clickOnChooseProjectLOV).click();
       Thread.sleep(900);
        driver.findElement(clickProjectNameBtn).click();
       Thread.sleep(900);
        driver.findElement(clickOnChooseActivityLOV).click();
        driver.findElement(clickActivityDailyScrum).click();
       Thread.sleep(900);
        driver.findElement(clickSaveBtn).click();
        driver.findElement(clickRowSaveBtn).click();
        Thread.sleep(900);
        System.out.println("Scrum call");
        //Iteration Execution
        driver.findElement(selectProjectBtn).click();
       Thread.sleep(900);
        driver.findElement(projectActivityWindowDisplayed).isDisplayed();
        driver.findElement(clickOnChooseProjectLOV).click();
       Thread.sleep(900);
        driver.findElement(clickProjectNameBtn).click();
       Thread.sleep(900);
        driver.findElement(clickOnChooseActivityLOV).click();
        driver.findElement(clickActivityIterationExecution).click();
       Thread.sleep(900);
        driver.findElement(clickSaveBtn).click();
        driver.findElement(clickRowSaveBtn).click();
        Thread.sleep(900);
        System.out.println("Execution");
    }
    public boolean clickOnSaveAsDraft() throws InterruptedException {
        driver.findElement(clickSaveasDraftBtn).click();
        Thread.sleep(1000);
        String msg1 = driver.findElement(msgDataSaved).getText();
        return msg1.equals("Data saved.");
    }

}
